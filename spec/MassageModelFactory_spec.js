describe('MessageModelFactory_Spec', function() {

  var MsgModel;

  beforeEach(module('chatapp'));

  beforeEach(inject(function(MessageModel) {
    MsgModel = MessageModel;
  }));

  describe('MessageModel が存在するか', function() {
    it('model exists', function() {
      expect(MsgModel).toBeDefined();
    });
  });

  describe('メソッドテスト', function() {
    var msg;
    beforeEach(function() {
      msg = new MsgModel('yuzu', 'これはテストですよ');
    });

    it('GET userName', function() {
      expect(msg.getUserName()).toBe('yuzu');
    });

    it('GET msgBody', function() {
      expect(msg.getBody()).toBe('これはテストですよ');
    });
  });
});