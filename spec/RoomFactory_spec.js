describe('RoomFactory_Spec', function() {
  var roomapi;

  beforeEach(module('chatapp'));

  beforeEach(inject(function(_Room_) {
    roomapi = _Room_;
  }));

  describe('ルーム名の長さテスト', function() {
    var normalRoomName,
      longRoomName;

    beforeEach(function() {
      normalRoomName = "aaaa";//正常な範囲
      longRoomName = "";
      for(var i = 0; i < 31; i++) {
        longRoomName += 'a';//文字数オーバーの名前を作る
      }
    });

    it('名前としてOKな長さ', function() {
      expect(roomapi.roomNameCheck(normalRoomName)).toBeTruthy();
    });

    it('名前が設定されてない0文字の時にエラー', function() {
      expect(roomapi.roomNameCheck('')).toBeFalsy();
    });

    it('名前が長すぎる時にエラーを出す', function() {
      expect(roomapi.roomNameCheck(longRoomName)).toBeFalsy();
    });
  });
});