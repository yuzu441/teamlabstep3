describe('UserNameFormFactory_Spec', function() {
  var unf,
    httpBackend;

  beforeEach(module('chatapp'));

  beforeEach(inject(function($httpBackend, UserNameForm) {
    httpBackend = $httpBackend;
    unf = UserNameForm;
  }));

  describe('UserNameForm が存在するか', function() {
    it('factory exists', function() {
      expect(unf).toBeDefined();
    });
  });

  describe('ユーザー名の長さテスト', function() {
    var normalName,
      longName;

    beforeEach(function() {
      normalName = "aaaa";//正常な範囲
      longName = "";
      for(var i = 0; i < 31; i++) {
        longName += 'a';//文字数オーバーの名前を作る
      }
    });

    it('名前としてOKな長さ', function() {
      expect(unf.userNameCheck(normalName)).toBeFalsy();
    });

    it('名前が設定されてない0文字の時にエラー', function() {
      expect(unf.userNameCheck('')).toBeTruthy();
    });

    it('名前が長すぎる時にエラーを出す', function() {
      expect(unf.userNameCheck(longName)).toBeTruthy();
    });
  });

  describe('ルームの存在チェック', function() {
    var roomId,
      roomCheckHandle;

    beforeEach(function() {
      roomId = 'Hallelujah Overdrive';
      roomCheckHandle = httpBackend.whenGET("/api/room/check/" + roomId)
    });

    it('roomId取り出し', function() {
      var roomHash = "#/" + roomId;
      expect(unf.getRoomId(roomHash)).toBe(roomId);
    });

    //TODO:httpBackend作る
    it('ルームがある場合', function() {
      roomCheckHandle.respond('');
      var promise = unf.roomExists('#/' + roomId),
        responseData = "";

      promise.then(function(data) {
        responseData = data;
      });

      httpBackend.flush();
      expect(responseData.msg).toBe('Room Exists');
      expect(responseData.roomId).toBe(roomId);
    });

    it('ルームがない場合', function() {
      roomCheckHandle.respond(404);
      var promise = unf.roomExists('#/' + roomId),
        responseUrl = '';

      var callback = function(d) {
        responseUrl = d;
      };

      promise.then(callback, callback);
      httpBackend.flush();
      expect(responseUrl).toBe("/views/error.html");
    })
  })
});