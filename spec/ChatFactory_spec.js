describe('ChatFactory_Spec', function() {
  var $httpBackend,
    ChatFactory;

  beforeEach(module('chatapp'));

  beforeEach(inject(function(_$httpBackend_, _Chat_){
    $httpBackend = _$httpBackend_;
    ChatFactory = _Chat_;
  }));

  describe('chatFactoryテスト', function() {
    var roomId,
      getLogHandler;

    beforeEach(function(){
      roomId = "557a7b2851490706b7411ad6";
      getLogHandler = $httpBackend.whenGET('/api/chat/getlog/' + roomId);
    });

    it('ログを取得できるかテスト', function() {
      var expectedObj = [
        {
          name: 'test1',
          body: 'hogehogehogehogehogehogehoge'
        },
        {
          name: 'test2',
          body: 'vim vs emacs'
        }
      ];

      getLogHandler.respond(expectedObj);
      var promise = ChatFactory.getLog(roomId),
        responseData = [];

      promise.then(function(data) {
        responseData = data;
      });

      $httpBackend.flush();
      expect(responseData).toEqual(expectedObj);
    });
  });
});