var gulp = require("gulp");
var gExpress = require("gulp-express");
var bower = require("gulp-bower");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var karma = require('karma').server;

var appFileName = 'app.js';

var filePath = {
  karmaConf: '/karma.conf.js'
};

gulp.task('heroku', ['jsBuild'], function() {
  var option = {};
  option.env = process.env;
  option.env.NODE_ENV_STATUS = 'heroku';

  gExpress.run(appFileName, option);
});

gulp.task('server', ['jsBuild','bower'], function() {
  var option = {};
  option.env = process.env;
  option.env.NODE_ENV_STATUS = 'myServer';

  gExpress.run(appFileName, option);
});

gulp.task('bower', function() {
  return bower();
});

gulp.task('jsBuild', function() {
  browserify('./app/js/main.js')
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('./app/dest/js'));
});

gulp.task('karma', ['jsBuild'], function(done) {
  karma.start({
    configFile: __dirname + filePath.karmaConf,
    singleRun: true
  }, done);
});