'use strict';

require('angular');
require('angular-sanitize');

angular.module('chatapp', ['ngSanitize']);

//Sarvice
require('./SocketFactory');
require('./MessageModelFactory');
require('./UserNameFormFactory');
require('./ChatFactory');
require('./ChatappFactory');
require('./RoomFactory');

//Controller
require('./TopRoomController');
require('./ChatController');