'use strict';

function ChatFactory($http, $q, $sanitize, ChatappFactory) {
  return {
    getLog: getLog,
    sanitizeMsgCheck: sanitizeMsgCheck,
    paragraph2br: paragraph2br
  };

  function getLog(roomId, callback) {
    var getLogUrl = '/api/chat/getlog/' + roomId;
    var d = $q.defer();

    $http.get(getLogUrl)
      .success(function(chatData) {
        d.resolve(chatData);
        ChatappFactory.sendCallback(callback, chatData);
      })
      .error(function(e) {
        d.reject(e);
        ChatappFactory.sendCallback(callback, e);
      });

    return d.promise;
  }

  /**
   * 改行を置換後、xssの可能性のあるコードをチェック
   * @param msgbody
   * @returns {*}
   */
  function sanitizeMsgCheck(msgbody) {
    var brText = paragraph2br(msgbody);
    return $sanitize(brText);
  }

  function paragraph2br(msgbody) {
    var reg = new RegExp("\\r?\\n", "g");
    return msgbody.replace(reg, "<br />");
  }
}

angular.module('chatapp').factory('Chat', ['$http', '$q', '$sanitize', 'ChatappFactory', ChatFactory]);