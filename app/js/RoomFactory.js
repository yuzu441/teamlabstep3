'use strict';

function RoomFactory($http, $q, ChatappFactory, callback) {
  return {
    createRoom: createRoom,
    roomNameCheck: roomNameCheck
  };

  function roomNameCheck(roomName) {
    return (roomName.length > 0 && roomName.length <= 30);
  }

  function createRoom(roomName) {
    var d = $q.defer();
    var sendData = {
      roomName: roomName
    };

    var createRoomApiUrl = '/api/room/create';

    $http.post(createRoomApiUrl, sendData)
      .success(function(data) {
        d.resolve(data);
        ChatappFactory.sendCallback(callback, data);
      })
      .error(function(data, status) {
        d.reject(status);
        ChatappFactory.sendCallback(callback, data);
      });

    return d.promise;
  }
}

angular.module('chatapp').factory('Room', ['$http', '$q', 'ChatappFactory', RoomFactory]);