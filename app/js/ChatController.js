'use strict';

function chatController($scope, $timeout, socket, MessageModel, UserNameForm, Chat) {
  $scope.reverseFlag = false;
  $scope.messages = [];
  $scope.username = "";
  $scope.userNameFormTxt = "";
  $scope.userNameFormDesc = "あなたの名前を入力してください";
  $scope.userNameIsError = true;
  $scope.roomId = "";
  $scope.msgFormText = "";


  $scope.userNameFormChanged = function() {
    var name = $scope.userNameFormTxt;
    $scope.userNameIsError = UserNameForm.userNameCheck(name);
  };

  $scope.decisionUserName = function() {
    UserNameForm.decisionUserName($scope.roomId, $scope.userNameFormTxt)
      .then(function(data) {
        //チェックOK
        var roomInfo = {
          username: data.name,
          roomId: data.roomId
        };
        $scope.username = data.name;
        socket.emit('setroom', roomInfo);
      })
      .catch(function(err) {
        if(err.status === 409) {
          $scope.userNameFormDesc = "同じ名前の人がいるのでログインできません";
        }
        else {
          $scope.userNameFormDesc = "ユーザ名は1〜30文字以内で入力してください"
        }
      });
  };

  $scope.formKeydownSubmit = function(event) {
    if(event.keyCode === 13 && event.metaKey) {
      $scope.msgSubmit();
    }
  };

  $scope.msgSubmit = function() {
    var formText = $scope.msgFormText;
    if(formText.length > 0) {
      var replaceBody = Chat.sanitizeMsgCheck(formText);
      var msg = {
        name: $scope.username,
        body: replaceBody
      };
      socket.emit('msgsend', msg);
      $scope.msgFormText = "";
    }
  };

  //websocketの設定
  socket.on('msgreceive', function(msg){
    $scope.messages.push(new MessageModel(msg.name, msg.body));
  });

  socket.on('reverse', function(msg) {
    if(msg.name === $scope.username) {
      $scope.reverseFlag = true;
      $timeout(function() {//指定時間後に画面反転を戻す
        $scope.reverseFlag = false;
      }, 3000)
    }
  });

  (function(){
    var roomHash = location.hash;
    UserNameForm.roomExists(roomHash)
      .then(function(data) {//room found
        $scope.roomId = data.roomId;
        Chat.getLog(data.roomId, function(chatData) {
          chatData.forEach(function(msg) {
            $scope.messages.push(new MessageModel(msg.name, msg.body));
          });
        });
      },
      function(errorUrl) {// room not found
        location.pathname = errorUrl;
      });

    //ハッシュが変わった時に画面を更新する
    addEventListener('hashchange', function() {
      location.reload();
    });
  })();
}

angular.module('chatapp').controller('chatController', ['$scope', '$timeout', 'socket', 'MessageModel', 'UserNameForm', 'Chat', chatController]);