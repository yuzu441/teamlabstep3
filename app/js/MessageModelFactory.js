'use strict';

function MessageModel() {

  function MessageModel(username, body) {
    this.username = username;
    this.body = body;
  }

  MessageModel.prototype.getUserName = function() {
    return this.username;
  };

  MessageModel.prototype.getBody = function() {
    return this.body;
  };

  return MessageModel;
}

angular.module('chatapp').factory('MessageModel', MessageModel);