'use strict';

function UserNameFormFactory($q, $http, ChatappFactory) {

  return {
    userNameCheck: userNameCheck,
    roomExists: roomExists,
    getRoomId: getRoomId,
    decisionUserName: decisionUserName
  };

  //startWithの実装
  if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
      position = position || 0;
      return this.lastIndexOf(searchString, position) === position;
    };
  }

  /**
   * ユーザー名の長さチェック
   * @param name
   * @returns {boolean}
   */
  function userNameCheck(name) {
    return (name.length <= 0 || name.length >= 30);
  }

  function roomExists(locationHash, callback) {
    var d = $q.defer(),
      errorUrl = "/views/error.html";

    if(!locationHash.startsWith('#/')) {
      //書式的と違う
      d.reject(errorUrl);
      ChatappFactory.sendCallback(callback, errorUrl);
    }
    else {
      var splitRoomHash = getRoomId(locationHash);
      var roomIdCheckUrl = '/api/room/check/' + splitRoomHash;

      $http.get(roomIdCheckUrl)
        .success(function() {
          var dataObj = {
            msg: 'Room Exists',
            roomId: splitRoomHash
          };
          d.resolve(dataObj);
          ChatappFactory.sendCallback(callback, dataObj);
        })
        .error(function() {
          //エラー時の処理
          d.reject(errorUrl);
          ChatappFactory.sendCallback(callback, errorUrl);
        });
    }

    return d.promise;
  }

  /**
   * locationHashからroomId部分を取得して返す
   * @param locationHash
   * @returns {String}
   */
  function getRoomId(locationHash) {
    return locationHash.split('/')[1];
  }

  function decisionUserName(roomId, userName, callback) {
    var checkUrl = '/api/room/login/' + roomId + '/'+ userName,
      d = $q.defer();
    $http.get(checkUrl)
      .success(function() {
        var retObj = {
          msg: 'Login Success',
          name: userName,
          roomId: roomId
        };
        d.resolve(retObj);
        ChatappFactory.sendCallback(callback, retObj);
      })
      .error(function(err, status) {
        var errObj = {
          status: status
        };
        d.reject(errObj);
        ChatappFactory.sendCallback(callback, errObj);
      });

    return d.promise;
  }
}

angular.module('chatapp').factory('UserNameForm', ['$q', '$http', 'ChatappFactory', UserNameFormFactory]);