'use strict';

function ChatappFactory() {
  return {
    sendCallback: sendCallback
  };

  /**
   * callbackがfunctionならコールバックを返す
   * @param callback
   * @param data
   */
  function sendCallback(callback, data) {
    if(typeof callback === 'function') { callback(data); }
  }
}

angular.module('chatapp').factory('ChatappFactory',[ChatappFactory]);