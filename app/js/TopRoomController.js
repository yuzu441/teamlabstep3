'use strict';

angular.module('chatapp').controller('roomController', function($scope, $http, Room) {
  $scope.createRoomFormText = "";
  $scope.formMessage = "作成するRoomの名前を入力してください";
  $scope.formError = false;
  $scope.roomUrl = "";

  $scope.createRoom = function() {
    var roomName = $scope.createRoomFormText;

    if(Room.roomNameCheck(roomName)) {
      Room.createRoom(roomName)
        .then(function(data){
          $scope.formError = false;
          $scope.roomUrl = "//" + location.host + "/views/chat.html#/" + data.roomId
        })
        .catch(function() {
          formDispError();
        });
    }
    else {
      formDispError();
    }
  };

  function formDispError() {
    $scope.formMessage = "Room名は1〜30文字以内で入力してください";
    $scope.formError = true;
  }
});