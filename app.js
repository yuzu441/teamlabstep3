(function() {
  'use strict';

  var express = require("express");
  var app = express();
  var http = require('http').Server(app);
  var io = require('socket.io')(http);
  var bodyParser = require('body-parser');
  var roomApi = require('./server_modules/roomapi');
  var chatApi = require('./server_modules/chatapi');

  app.use(bodyParser.json());

  app.get("/", function(req, res) {
    res.sendFile(__dirname + "/app/views/index.html");
  });

  //ルーム作成
  app.post("/api/room/create", roomApi.createRoom);

  //ルームチェック
  app.get("/api/room/check/:roomId", roomApi.roomCheck);

  //名前確認
  app.get("/api/room/login/:roomId/:userName", roomApi.addUserInRoom);

  //ルームのログ取得
  app.get("/api/chat/getlog/:roomId", chatApi.getLog);

  app.use(express.static("bower_components"));
  app.use(express.static("app"));

  io.on('connection', function(socket) {
    console.log('a user connected');
    var room;

    socket.on('setroom', function(roomInfo) {
      room = {
        roomId: roomInfo.roomId,
        name: roomInfo.username
      };
      socket.join(room.roomId);
    });

    socket.on('msgsend', function(msg) {
      var reverseReg = new RegExp('^@(\\S{1,30})\\sreverse$');
      var regBody = msg.body.match(reverseReg);
      if(regBody) {
        socket.to(room.roomId).emit('reverse', {name: regBody[1]});
        return
      }

      var replaceAlink = chatApi.replaceAlink(msg);

      chatApi.saveChatMsg(room.roomId, replaceAlink)
        .then(function(data) {
          io.to(room.roomId).emit('msgreceive', msg);
        })
        .catch(function(data) {
          socket.emit('msgSavaError', data);
        })
    });

    socket.on('disconnect', function() {
      console.log('user disconnected');
      if(typeof room !== "undefined") { roomApi.logoutRoom(room.roomId, room.name); }
    });
  });

  var port = process.env.PORT || 3000;
  http.listen(port, function() {
    console.log('listening on *:3000');
  })
})();