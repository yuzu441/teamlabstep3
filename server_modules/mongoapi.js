(function(){
  'use strict';

  var mongoose = require('mongoose');
  mongoose.connect('mongodb://localhost/teamlab3');//TODO: heroku対応調べる
  var Schema = mongoose.Schema;

  var UserSchema = new Schema({
    name: {type: String, required: true}
  });
  mongoose.model('UserModel', UserSchema);

  var RoomSchema = new Schema({
    name: {type: String, required: true},
    roomMember: ['UserModel']
  });
  mongoose.model('RoomModel', RoomSchema);

  var ChatMsgSchema = new Schema({
    name: {type: String, required: true},
    body: {type: String, required: true},
    roomId: String
  });
  mongoose.model('ChatMsgModel', ChatMsgSchema);

  function createRoom(roomName) {
    var RoomModel = mongoose.model('RoomModel');
    var newRoom = new RoomModel({
      name: roomName,
      roomMember: []
    });

    return new Promise(function(resolve, reject) {
      newRoom.save(function(err, saveNewRoom) {
        err ? reject(err) : resolve(saveNewRoom._id);
      })
    });
  }

  function roomCheck(roomId, callback) {
    //room is exists
    var RoomModel = mongoose.model('RoomModel');
    return RoomModel.findOne({_id: roomId}).exec();
  }

  function addRoomMember(roomId, userName) {
    //search
    return new Promise(function(resolve, reject) {
      var RoomModel = mongoose.model('RoomModel');
      RoomModel.findOne({_id: roomId})
        .exec()
        .then(function(room) {
          if(!room) {
            //ルームが見つからない場合はnull
            reject({
              status: 403
            });
          }

          var result = room.roomMember.every(function(inUser) {
            //ログイン中のユーザーに同じ名前の人がいない
            return inUser.name !== userName;
          });
          if(!result) {
            reject({
              status: 409
            });
          }
          else {
            var UserModel = mongoose.model('UserModel');
            var user = new UserModel({
              name: userName
            });

            room.roomMember.push(user);
            room.save(function(err, room) {
              err ? reject(err) : resolve(room);
            })
          }
        })
        .onReject(function(err) {
          if(err.name === "CastError") {
            err.status = 403;
            reject(err);
            return
          }
          reject(err);
        });
    });
  }

  function removeRoomMember(roomId, userName) {
    //remove
    var RoomModel = mongoose.model('RoomModel');

    return new Promise(function(resolve, reject) {
      RoomModel.findOne({_id: roomId})
        .exec()
        .then(function(room) {
          room.roomMember = room.roomMember.filter(function(m) {
            return userName !== m.name;
          });
          room.save(function(err, room) {
            err ? reject(err) : resolve(room);
          })
        })
    });
  }

  function getChatLog(roomId) {
    //return log
    var ChatMsgModel = mongoose.model('ChatMsgModel');
    return ChatMsgModel.find({roomId: roomId}).exec();
  }

  function chatSave(roomId, msg) {
    return new Promise(function(resolve, reject) {
      var ChatMsgModel = mongoose.model('ChatMsgModel');
      var newMsg = new ChatMsgModel({
        name: msg.name,
        body: msg.body,
        roomId: roomId
      });
      newMsg.save(function(err) {
        if(err) {
          //保存失敗
          reject(newMsg);
        }
        else {
          resolve(newMsg);
        }
      });
    })
  }

  module.exports = {
    createRoom: createRoom,
    roomCheck: roomCheck,
    addRoomMember: addRoomMember,
    removeRoomMember: removeRoomMember,
    getChatLog: getChatLog,
    chatSave: chatSave
  };
})();