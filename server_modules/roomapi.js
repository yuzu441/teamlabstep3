(function() {
  'use strict';

  var mongoapi = process.env.NODE_ENV_STATUS != 'heroku'
    ? require('./mongoapi')
    : require('./dummymongoapi');

  function createRoom(req, res) {
    //プロパティがない
    if(req.body.hasOwnProperty("roomName") != true) {
      res.status(400);
      res.send();
      return
    }

    var roomName = req.body.roomName;
    if(roomName.length <= 0 || roomName.length > 30) {
      //文字数オーバー
      res.status(406);
      res.send();
      return
    }

    mongoapi.createRoom(roomName)
      .then(function(roomId) {
        res.status(201);
        res.json({
          roomName: roomName,
          roomId: roomId
        })
      })
      .catch(function(err) {
        res.status(500);
        res.json(err);
      });
  }

  function roomCheck(req, res) {
    var roomId = req.params.roomId;

    mongoapi.roomCheck(roomId)
      .then(function(data) {
        if(data) {
          res.json({result: "Room Check OK"});
        }
        else {
          res.status(404);
          res.json({result: "Room Check Error"});
        }
      })
      .onReject(function() {
        res.status(400);
        res.json({result: "Room Check Error"});
      });
  }

  function addUserInRoom(req, res) {
    var roomId = req.params.roomId;
    var name = req.params.userName;

    mongoapi.addRoomMember(roomId, name)
      .then(function() {
        res.json({result: 'Room Logined'});
      })
      .catch(function(err) {
        var resultObj = {};
        switch(err.status) {
          case 403:
            resultObj.result = 'Forbidden'; break;
          case 409:
            resultObj.result = 'Conflict'; break;
          default :
            resultObj.result = 'Internal Server Error';
        }
        res.status(err.status || 500);
        res.json(resultObj);
      });
  }

  function logoutRoom(roomId, name) {
    mongoapi.removeRoomMember(roomId, name);
  }

  module.exports = {
    createRoom: createRoom,
    roomCheck: roomCheck,
    addUserInRoom: addUserInRoom,
    logoutRoom: logoutRoom
  };
})();