(function(){
  'use strict';
  var crypto = require('crypto');

  var room = {};

  function createRoom(roomName) {
    return new Promise(function(resolve) {
      var preHashMsg = roomName + new Date();
      var md5 = crypto.createHash('md5');
      md5.update(preHashMsg);
      var hash = md5.digest('hex');
      resolve(hash);
    });
  }

  function roomCheck(roomId) {
    //room is exists
    return new Promise(function(resolve) {
      if(!room.hasOwnProperty(roomId)) {
        room[roomId] = {
          member: []
        };
      }
      resolve(true);
    })
  }

  function addRoomMember(roomId, userName) {
    return new Promise(function(resolve, reject) {
      var isExists = room[roomId].member.some(function(user) {
        return user.name === userName;
      });
      if(isExists) { //同じ名前の人がいた
        reject(409);
      }
      else {
        room[roomId].member.push({
          name: userName
        });
        resolve();
      }
    });
  }

  function removeRoomMember(roomId, userName) {
    if(typeof room[roomId].member !== "undefined") {
      room[roomId].member = room[roomId].member.filter(function(user) {
        return user.name !== userName;
      });
      if(room[roomId].member.length === 0) { delete room[roomId];}
    }
  }

  function getChatLog() {
    return new Promise(function(resolve) {
      resolve();//heroku上ではログ保存してないため返すものはない
    });
  }

  function chatSave() {
    return new Promise(function(resolve) {
      resolve();//heroku上でchatを保存しない
    })
  }

  module.exports = {
    createRoom: createRoom,
    roomCheck: roomCheck,
    addRoomMember: addRoomMember,
    removeRoomMember: removeRoomMember,
    getChatLog: getChatLog,
    chatSave: chatSave
  };
})();