(function(){
  'use strict';

  var mongoapi = process.env.NODE_ENV_STATUS != 'heroku'
    ? require('./mongoapi')
    : require('./dummymongoapi');

  function getLog(req, res) {
    var roomId = req.params.roomId;

    mongoapi.getChatLog(roomId)
      .then(function(data) {
        if(data) {
          var resData = data.map(function(chatObj) {
            return {
              name: chatObj.name,
              body: chatObj.body,
              roomId: chatObj.roomId
            };
          });
          res.json(resData);
        }
      });
  }

  /**
   * httpリンクにaタグ入れる
   * @param msg
   * @returns {*}
   */
  function replaceAlink(msg) {
    var reg = new RegExp('(htt(p|ps)://.+)', 'g');
    var replace = msg.body.replace(reg, '<a href="$1">$1</a>');
    var newMsg = msg;
    newMsg.body = replace;
    return newMsg;
  }

  function saveChatMsg(roomId, msg) {
    return mongoapi.chatSave(roomId, msg)
  }

  module.exports = {
    getLog: getLog,
    saveChatMsg: saveChatMsg,
    replaceAlink: replaceAlink
  };
})();