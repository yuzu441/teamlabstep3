# teamlabオンラインスキルアップチャレンジ Step3

----------------------------

## 注意
__クレジットカードがないとHeroku上でMongoDBが使用できないため__  
__Heroku用の起動分とローカル用の起動分の２パターンの起動があります__

## Chatアプリ

[Heroku上DEMO](https://safe-taiga-4039.herokuapp.com/)

[チームラボオンラインスキルアップチャレンジ](http://team-lab.github.io/skillup-nodejs/)  
[Step3 最終課題](http://team-lab.github.io/skillup-nodejs/3/7.html)

### 動作環境

以下のブラウザは動作確認をしました

  + Google Chrome 43.0.2357.81 (64-bit)

### 動作方法

    git clone git@bitbucket.org:yuzu441/teamlabstep3.git
    npm install
    gulp server #ローカル版 - 起動時にMongodb必要
    gulp heroku #heroku版

### 制作環境と使用技術

#### 制作環境

  + WebStorm 10.0.3

#### 使用技術 & フレームワーク

  + Javascript
    - Node.js
    - Express
    - Angular.js
    - socket.io
    - Mongoose
  + ビルドツール & パッケージ管理
    - npm
    - bower
    - gulp
  + CSS
    - SkyBlue.css
  + DB
    - MongoDB
    
### イースターエッグコマンド

#### 相手の画面を3秒間反転させる
`@username reverse`